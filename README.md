# Instituto Infnet
# Projeto de Bloco - Computação e Inteligência Artificial
Felipe, João, Leonardo, Suellen

# Instalando
pip install -r requirements.txt

# Slitherin
AI research environment for the game of Snake written in Python 2.7. Part of the [OpenAI - Request For Research 2.0](https://blog.openai.com/requests-for-research-2/).

## Rules
1. Snake has to move either forward, left or right.
2. Snake dies when hits wall or itself.
3. For every eaten fruit, snake's length increases by 1 and a new fruit is generated on a random unoccupied place.

## Game Author
**Greg (Grzegorz) Surma**
