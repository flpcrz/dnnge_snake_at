# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 14:06:42 2020

@author: FOC
"""

import os
import pandas as pd 
import matplotlib.pyplot as plt
from sklearn import preprocessing


## Comparado os resultado dos jogos AI x Humano ##
jogos_AI = pd.read_csv('./scores/deep_neural_net.csv', delim_whitespace=True, header=0)
jogos_HU = pd.read_csv('./scores/human.csv', delim_whitespace=True, header=0)
jogos_HU.columns = ['Humano']  
jogos_AI.columns = ['AI'] 

_, ax = plt.subplots()
# Partidas x Resultado AI
jogos_AI.plot(ax=ax)
# Partidas x Resultado HUMANO
jogos_HU.plot(ax=ax)

# Media, Min, Max
print('#############')
print('Total partidas AI:', jogos_AI.size)      
print('AI -','Média:', jogos_AI['AI'].mean(), 'Min:',  jogos_AI['AI'].min(), 'Max:',  jogos_AI['AI'].max())
print('#############')
print('Total partidas Humano:', jogos_HU.size)   
print('Humano -', 'Média:', jogos_HU['Humano'].mean(), 'Min:',  jogos_HU['Humano'].min(), 'Max:',  jogos_HU['Humano'].max())
