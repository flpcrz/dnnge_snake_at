from models.dnn_base_game_model import DNNBaseGameModel

class DNNPlayer(DNNBaseGameModel):

    def __init__(self):
        DNNBaseGameModel.__init__(self, "Deep Neural Net", "deep_neural_net", "dnn")

    def move(self, environment):
        DNNBaseGameModel.move(self, environment)
        return self._predict(environment, self.model)