import numpy as np
from game.environment.action import Action
from game.environment.environment import Environment
from game.helpers.constants import Constants
from models.base_game_model import BaseGameModel
from models.dnn_model import DeepNeuralNetModel

class DNNBaseGameModel(BaseGameModel):

    model = None

    def __init__(self, long_name, short_name, abbreviation):
        BaseGameModel.__init__(self, long_name, short_name, abbreviation)

    def move(self, environment):
        if self.model is None:
            self.model = DeepNeuralNetModel(Constants.MODEL_DIRECTORY)

        BaseGameModel.move(self, environment)
    
    # Prepara o ambiente de treino sem a interface gráfica
    def prepare_training_environment(self, horizontal_pixels = Constants.ENV_WIDTH, vertical_pixels = Constants.ENV_HEIGHT):
        environment = Environment(width = horizontal_pixels, height = vertical_pixels)
        environment.set_wall()
        environment.set_fruit()
        environment.set_snake()
        return environment

    def _predict(self, environment, model):
        # Cria uma lista das predições para cada ação possível       
        predictions = []   

        # Define as ações possíveis no ambiente:
        # Movimentos: esquerda = (-1, 0) cima = (0, -1) direita = (1, 0) baixo = (0, 1)
        # O movimento pode ser mantido: actions[1]
        # Pode virar para o viziho da esquerda ou direita: actions[0], actions[2]
        # A cobra não pode ir no sentido oposto do movimento
        actions = [
            Action.left_neighbor(environment.snake_action), 
            environment.snake_action,
            Action.right_neighbor(environment.snake_action)
        ]
        
        # Para cada ação possível, observar o ambiente nessa ação:
        i = 0
        for action in actions:
            i += 1

            # Cria a observação do ambiente do jogo com a ação possível
            observation_for_prediction = environment.observation(action)

            # Realiza a predição para a ação e adiciona a uma lista
            predictions.append(model.model.predict(np.array([observation_for_prediction]))) 
            
        # Retorna o índice de valor máximo para cada predict de cada ação
        best_prediction_index = np.argmax(np.array(predictions)) 
              
        # Resgata a ação de melhor resultado
        return actions[best_prediction_index]