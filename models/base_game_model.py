import csv
import os
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from collections import OrderedDict
from game.helpers.node import Node
from game.environment.environment import Environment

class BaseGameModel:

    def __init__(self, long_name, short_name, abbreviation):
        self.abbreviation = abbreviation
        self.long_name = long_name
        self.short_name = short_name

    def move(self, environment):
        # Indo de:
        self.starting_node = Node(environment.snake[0])
        # Ação:
        self.starting_node.action = environment.snake_action
        # Para:        
        self.fruit_node = Node(environment.fruit[0])

    def user_input(self, event):
        pass

    # Salva os scores em um csv para futuras avaliações
    def log_score(self, score):
        path = "scores/" + self.short_name + ".csv"
        print(path)
        if not os.path.exists(path):
            with open(path, "w"):
                pass
        scores_file = open(path, "a")
        with scores_file:
            writer = csv.writer(scores_file)
            writer.writerow([score])

    # Lê os scores do csv para analise imediata
    def stats(self):
        path = "scores/" + self.short_name + ".csv"
        if not os.path.exists(path):
            return "(?, ?, ?)"
        scores_file = open(path, "r")
        scores = []
        with scores_file:
            reader = csv.reader(scores_file)
            for row in reader:
                if (row):
                    scores.append(float(row.pop()))
        scores = list(map(lambda x: x, scores))
        minimum = min(scores)
        average = round(sum(scores)/float(len(scores)), 1)
        maximum = max(scores)
        return "("+str(minimum)+"/"+str(average)+"/"+str(maximum)+")"

    def reset(self):
        pass
