
import random
import copy
import time
import os
import sys
import csv
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint
from datetime import datetime
from statistics import mean
from models.dnn_base_game_model import DNNBaseGameModel
from game.helpers.constants import Constants

class DNNTrainer(DNNBaseGameModel):
    
    def __init__(self):
        DNNBaseGameModel.__init__(self, "Deep Neural Net", "deep_neural_net_trainer", "dnnt")

    def move(self, environment):
        DNNBaseGameModel.move(self, environment)
        
        
        start_time = time.time()
        self._train(2400, start_time)
        print("O treino durou {x} segundos".format(x = round(time.time() - start_time, 2)))

        self.log_score_autotest(self._test(3, start_time))

    def _train(self, training_runs, time):
        training_data = np.array(self._training_observations(training_runs))        
        x = np.array([i[0] for i in training_data])
        y = np.array([i[1] for i in training_data])  

        unique, counts = np.unique(y, return_counts=True)
        rewards = dict(zip(unique, counts))
        
        ''' checkpoint = ModelCheckpoint(
            os.path.join(Constants.MODEL_DIRECTORY, f"{str(int(time))}-{training_runs}-{len(training_data)}-{rewards[0.7]}-{rewards[0.1]}-{rewards[-0.2]}", "{epoch:03d}-{val_loss:03f}-{val_mse:03f}-{val_mae:03f}"),
            monitor = 'val_loss',
            save_best_only = True,
            mode = 'auto',
            restore_best_weights = True
        ) '''

        # Configura o log do Tensorboard
        logdir = ".\\logs\\scalars\\" + datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir = logdir)

        self.model.model.fit(x, y, epochs = 16, validation_split = 0.4, callbacks = [tensorboard_callback])
        self.model.save()
        
    def _test(self, testing_runs, time):
        moves_per_run = []
        eat_fruit_moves = 0
        good_moves = 0
        bad_moves = 0
        death_moves = 0

        print("Iniciando Teste")

        for i in range(0, testing_runs):
            # Cria um ambiente de jogo 10 x 10 para realizar o treino
            environment = self.prepare_training_environment(10, 10)

            # Prepara o jogo teste para a cobra testing_runs vezes
            print("Teste", i + 1)
            test_moves = [0, 0, 0, 0]  

            while True:                              
                # Faz um predict nesse ambiente criado
                predicted_action = self._predict(environment, self.model)
                
                # Antes da ação:
                # Retorna a distancia da cabeça da cobra até a fruta 
                pre_distance_from_fruit = environment.distance_from_fruit()

                # Retorna o tamanho da cobra
                pre_length = environment.reward()

                # Se morreu ou está em loop sem achar a fruta
                # .step(predicted_action) retorna false se a cobra colidiu no seu corpo ou em uma parede                
                if not environment.step(predicted_action) or environment.is_in_fruitless_cycle():                    
                    death_moves += 1   
                    print("\tMorreu")
                    break

                # Se a cabeça da cobra estiver na mesma posição da fruta, come a fruta 
                # Adiciona um segmento no corpo da cobra e cria outra fruta
                environment.eat_fruit_if_possible()

                # Depois da ação:
                # Retorna a distancia da cabeça da cobra até a fruta 
                post_distance_from_fruit = environment.distance_from_fruit()

                # Retorna o tamanho da cobra
                post_length = environment.reward()

                # Resultados da observação da jogada
                # Se a cobra comeu a fruta
                if post_length > pre_length:                    
                    eat_fruit_moves += 1
                    test_moves[0] += 1
                                       
                # Se ficou mais próixma da fruta
                elif post_distance_from_fruit < pre_distance_from_fruit:                    
                    good_moves += 1
                    test_moves[1] += 1   
                # Se não morreu
                else:                    
                    bad_moves += 1
                    test_moves[2] += 1

                test_moves[3] += 1

                sys.stdout.write(f'\rComeu a fruta: {test_moves[0]} - Chegou mais perto: {test_moves[1]} - Só não morreu: {test_moves[2]} - {test_moves[3]} observações')
                sys.stdout.flush() 

            # Lista com os resultados do score (tamanho do corpo da cobra)
            # Para as runs da cobra
            moves_per_run.append(environment.reward())

        mean_score = round(mean(moves_per_run), 2)

        print('---------------------------------------------------------')
        print('----------------- RESULTADO DOS TESTES: -----------------')
        print('Quantidade de partidas: {x}'.format(x = testing_runs))
        print('Score médio: {x}'.format(x = mean_score))
        print('Comeu a fruta: {x} vezes'.format(x = eat_fruit_moves))
        print('Chegou mais perto da fruta: {x}'.format(x = good_moves))
        print('Não morreu: {x} vezes'.format(x = bad_moves))
        print('Morreu: {x} vezes'.format(x = death_moves))
        print('---------------------------------------------------------')
        
        return [int(time), testing_runs, mean_score, eat_fruit_moves, good_moves, bad_moves]     

    def _training_observations(self, training_runs):
        print('Iniciando Treino com {x} partidas'.format(x = training_runs))

        # Cria um ambiente de jogo 10 x 10 para realizar o treino
        new_environment = self.prepare_training_environment(10, 10)
        rewards = []

        # Cria uma observações aleatórias
        print('Criando partidas...')
        train_moves = [0, 0, 0]
        for run_index in range(0, training_runs):
            environment = copy.deepcopy(new_environment)
            rewards_for_run = []     

            while True:
                # Cria uma ação possível randômica
                random_action = random.choice(environment.possible_actions_for_current_action(environment.snake_action))

                # Faz uma observação do ambiente com essa ação
                environment_observation = environment.observation(random_action)
                
                # Antes da ação:
                # Retorna a distancia da cabeça da cobra até a fruta 
                pre_distance_from_fruit = environment.distance_from_fruit()

                # Retorna o tamanho da cobra
                pre_length = environment.reward()

                # Realiza a ação no ambiente
                # Retorna false se a cobra colidiu no seu corpo ou em uma parede
                alive = environment.step(random_action)
                if not alive:
                    break

                # Se a cabeça da cobra estiver na mesma posição da fruta, come a fruta 
                # Adiciona um segmento no corpo da cobra e cria outra fruta
                environment.eat_fruit_if_possible()

                # Depois da ação:
                # Retorna a distancia da cabeça da cobra até a fruta 
                post_distance_from_fruit = environment.distance_from_fruit()

                # Retorna o tamanho da cobra
                post_length = environment.reward()

                # Resultados da observação da jogada
                # Se a cobra comeu a fruta, ganha uma recompensa de 0.7
                # Se ficou mais próixma da fruta, ganha uma reconpensa de 0.1
                # Se não morreu, é punida com -0.2
                if post_length > pre_length:
                    reward = [environment_observation, [0.7]]
                    train_moves[0] += 1
                elif post_distance_from_fruit < pre_distance_from_fruit:
                    reward = [environment_observation, [0.1]]
                    train_moves[1] += 1
                else:
                    reward = [environment_observation, [-0.2]]
                    train_moves[2] += 1                
                
                rewards_for_run.append(reward)

            # Se morreu, é punida com -1.0
            reward = [environment_observation, [-1.0]]
            rewards_for_run.append(reward)
            rewards += rewards_for_run

        print('---------------------------------------------------------')
        print('--------------- RESULTADO DAS OBSERVAÇÕES: --------------')
        print('Comeu a fruta: {reward}'.format(reward = train_moves[0]))
        print('Chegou mais perto da fruta: {reward}'.format(reward = train_moves[1]))
        print('Não morreu: {reward}'.format(reward = train_moves[2]))
        print('Total de observações: {reward}'.format(reward = len(rewards)))
        print('---------------------------------------------------------')

        return rewards

    def log_score_autotest(self, rewards):
        path = "score_autotest/tests_dnn.csv"
        if not os.path.exists(path):
            with open(path, "w"):
                pass
        scores_file = open(path, "a")
        with scores_file:
            writer = csv.writer(scores_file)
            writer.writerow(rewards)