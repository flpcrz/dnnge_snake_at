import os
import os.path
import tensorflow as tf
from game.helpers.constants import Constants

class DeepNeuralNetModel:   
    
    def __init__(self, path):
        # Resgada o path e o arquivo do modelo        
        self.dnn_model_path = path
        self.dnn_model_file_name = os.path.join(self.dnn_model_path, Constants.MODEL_NAME)

        # Define a camada de entrada com 5 neurônios
        self.model = tf.keras.models.Sequential([
            tf.keras.layers.Input(shape=(5,)),
            tf.keras.layers.Dense(125, activation = tf.nn.relu),
            tf.keras.layers.Dense(1),
        ])

        # Adam
        adam_optimizer = tf.keras.optimizers.Adam(
            learning_rate = 0.01,
            beta_1 = 0.9,
            beta_2 = 0.99,
            epsilon = 1e-05,
            amsgrad = False,
            name = 'Adam'
        )

        # RMSprop
        RMSpro_optimizer = tf.keras.optimizers.RMSprop(0.0099)
        self.model.compile(loss = 'mse', optimizer = adam_optimizer, metrics = ['mse', 'mae'])
        
        self.load(self.dnn_model_file_name)

    def load(self, path):
        # Caso já exista um modelo, carrega o mesmo
        if os.path.isfile(path):
            print('Loading')
            self.model = tf.keras.models.load_model(os.path.join(self.dnn_model_path))

    def save(self):
        print('Saving')
        self.model.save(os.path.join(self.dnn_model_path))
